package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        } else if (command.equals(Command.Left)) {
            left();
        } else if (command.equals(Command.Right)) {
            right();
        }
    }

    private void right() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.values()[1]); break;
            case East:
                location.setDirection(Direction.values()[2]); break;
            case South:
                location.setDirection(Direction.values()[3]); break;
            case West:
                location.setDirection(Direction.values()[0]); break;
            default: break;
        }
    }

    private void left() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.values()[3]); break;
            case East:
                location.setDirection(Direction.values()[0]); break;
            case South:
                location.setDirection(Direction.values()[1]); break;
            case West:
                location.setDirection(Direction.values()[2]); break;
            default: break;
        }
    }

    private void move() {
        switch (location.getDirection()) {
            case North:
                location.setCoordinateY(location.getCoordinateY() + 1); break;
            case East:
                location.setCoordinateX(location.getCoordinateX() + 1); break;
            case South:
                location.setCoordinateY(location.getCoordinateY() - 1); break;
            case West:
                location.setCoordinateX(location.getCoordinateX() - 1); break;
            default: break;
        }
    }

    public Location getLocation() {
        return location;
    }

    public void executeBatchCommands(List<Command> commands) {
        commands.stream().forEach(this::executeCommand);
    }
}
