Objective:
1.	We started to learn TTD. In the morning, we tried to write unit testing, which contains test description, GIVEN, WHEN and THEN.
2.	We did code review, after which I made the showcase of my codes written yesterday. 
3.	We practiced to finish the FizzBuzz system using TDD, before which I created a live template of test methods

Reflective:
1.	Learning TDD is an exciting thing for me, because I learned that it is a good action to improve the code quality and help the company to save a large amount of testing cost.
2.	When chosen to show my codes by my teammates, I am a little nervous because I have no experience so that I do not know whether I can do it well. I neither know Whether I have clarified my codes clearly.

Interpretive:
1.	Why I had not taken testing as my first step before developing, I met a lot of trouble when I had to fix some bugs in my previous coding career. Now, gaining the knowledge of TDD really exciting me.
2.	Everything is difficult at the start, the nervous mood is produced because I thought it hard.

Decisional:
1.	I think that I should do more deliberately practice to improve my TDD skills and coding speed. 
2.	I decided to improve my expression ability which does good to the cooperation with my teammates and is important in my coding career.
